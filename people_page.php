<?php
/**
* Template Name: Faculty Profile from user profile
 *
 * @package WF College Two
 */

get_header(); ?>

<?php 
include(dirname(__FILE__) . '/pp_functions.php');
?>

 

 
 
 

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
			
			



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="edudms-entry-content">
	
	
	<?php
	
	if (get_option('edudms_pt_pp_layout_piece_top_text_setting') == '1' ) {
		echo '<div class="pp_header_top_text">';
		echo wpautop(get_option('edudms_pt_pp_top_text_editor'));
		echo '</div>';
	}
	
	
	
	if (get_option('edudms_pt_pp_layout_piece_1_setting') == '1' ) {
		
		echo '<div class="label3">Faculty</div>';
		
	$args = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => '',
	'meta_key'     => 'edudms_pt_member_type',
	'meta_value'   => 'Faculty',
	'meta_compare' => '',
	'meta_query'   => array(),
	'date_query'   => array(),        
	'include'      => array(),
	'exclude'      => array(),
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => '',
 ); 

$edudms_pt_person = get_users( $args );
	
	
	
	edudms_pt_header_start();
	//Create Header Divs Here
	edudms_pt_make_header('edudms_pt_pp_block_1_selection');
	edudms_pt_make_header('edudms_pt_pp_block_2_selection');
	edudms_pt_make_header('edudms_pt_pp_block_3_selection');
	edudms_pt_make_header('edudms_pt_pp_block_4_selection');
	edudms_pt_make_header('edudms_pt_pp_block_5_selection');
	edudms_pt_make_header('edudms_pt_pp_block_6_selection');
	edudms_pt_make_header('edudms_pt_pp_block_7_selection');
	//No more Header Divs beyond here
	edudms_pt_header_end();
	
	
usort($edudms_pt_person, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));	
	foreach ( $edudms_pt_person as $user ) {
		$user_identifier = $user->id;
		$profile_template_page = get_option('edudms_pt_profile_page_selection');
		$profile_link = get_permalink( $profile_template_page ) . '?user=' . $user_identifier;
		$profile_blob = '<a href="' . $profile_link .  '">';
		$profile_blob2 = esc_html($profile_blob);
		$first_name = $user->first_name;
		$last_name = $user->last_name;
		$title = $user->edudms_pt_title;
		$email = $user->user_email;
		$website = $user->user_url;
		$phone = $user->edudms_pt_phone;
		$office = $user->edudms_pt_office;
		$edudms_pt_bio = $user->edudms_pt_bio;
		$edudms_pt_cv = $user->edudms_pt_cv;
		$edudms_pt_courses = $user->edudms_pt_courses;
		$full_name = $first_name . ' ' . $last_name;
		$comma_name = $last_name . ', ' . $first_name;
		$edudms_pt_customshortfield1 = $user->edudms_pt_customshortfield1;
		$edudms_pt_customshortfield2 = $user->edudms_pt_customshortfield2;

		
	edudms_pt_person_setup_block('block_1_value', 'block_1_data_type', 'edudms_pt_pp_block_1_selection');
	edudms_pt_person_setup_block('block_2_value', 'block_2_data_type', 'edudms_pt_pp_block_2_selection');
	edudms_pt_person_setup_block('block_3_value', 'block_3_data_type', 'edudms_pt_pp_block_3_selection');
	edudms_pt_person_setup_block('block_4_value', 'block_4_data_type', 'edudms_pt_pp_block_4_selection');
	edudms_pt_person_setup_block('block_5_value', 'block_5_data_type', 'edudms_pt_pp_block_5_selection');
	edudms_pt_person_setup_block('block_6_value', 'block_6_data_type', 'edudms_pt_pp_block_6_selection');
	edudms_pt_person_setup_block('block_7_value', 'block_7_data_type', 'edudms_pt_pp_block_7_selection');
	edudms_pt_person_start();
	edudms_pt_wrap_it_start('edudms_pt_pp_name_and_title_layout_selection');
	edudms_pt_show_it($block_1_value, $block_1_data_type, 2 , get_option('edudms_pt_pp_block_1_linkon_selection'));
	edudms_pt_show_it($block_2_value, $block_2_data_type, 2 , get_option('edudms_pt_pp_block_2_linkon_selection'));
	edudms_pt_wrap_it_end('edudms_pt_pp_name_and_title_layout_selection');
	edudms_pt_show_it($block_3_value, $block_3_data_type, 2 , get_option('edudms_pt_pp_block_3_linkon_selection'));
	edudms_pt_show_it($block_4_value, $block_4_data_type, 2 , get_option('edudms_pt_pp_block_4_linkon_selection'));
	edudms_pt_show_it($block_5_value, $block_5_data_type, 2 , get_option('edudms_pt_pp_block_5_linkon_selection'));
	edudms_pt_show_it($block_6_value, $block_6_data_type, 2 , get_option('edudms_pt_pp_block_6_linkon_selection'));
	edudms_pt_show_it($block_7_value, $block_7_data_type, 2 , get_option('edudms_pt_pp_block_7_linkon_selection'));
	edudms_pt_person_end();
	}
	}
	?>


<?php
	
	
	if (get_option('edudms_pt_pp_layout_piece_2_setting') == '1' ) {

		echo '<div class="label3">Staff</div>';

	
$args = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => '',
	'meta_key'     => 'edudms_pt_member_type',
	'meta_value'   => 'staff',
	'meta_compare' => '',
	'meta_query'   => array(),
	'date_query'   => array(),        
	'include'      => array(),
	'exclude'      => array(),
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => '',
 ); 

$edudms_pt_person = get_users( $args );
	
	
	edudms_pt_header_start();
	//Create Header Divs Here
	edudms_pt_make_header('edudms_pt_pp_block_1_selection');
	edudms_pt_make_header('edudms_pt_pp_block_2_selection');
	edudms_pt_make_header('edudms_pt_pp_block_3_selection');
	edudms_pt_make_header('edudms_pt_pp_block_4_selection');
	edudms_pt_make_header('edudms_pt_pp_block_5_selection');
	edudms_pt_make_header('edudms_pt_pp_block_6_selection');
	edudms_pt_make_header('edudms_pt_pp_block_7_selection');
	//No more Header Divs beyond here
	edudms_pt_header_end();
	
	
usort($edudms_pt_person, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));	
	foreach ( $edudms_pt_person as $user ) {
		$user_identifier = $user->id;
		$profile_template_page = get_option('edudms_pt_profile_page_selection');
		$profile_link = get_permalink( $profile_template_page ) . '?user=' . $user_identifier;
		$profile_blob = '<a href="' . $profile_link .  '">';
		$profile_blob2 = esc_html($profile_blob);
		$first_name = $user->first_name;
		$last_name = $user->last_name;
		$title = $user->edudms_pt_title;
		$email = $user->user_email;
		$website = $user->user_url;
		$phone = $user->edudms_pt_phone;
		$office = $user->edudms_pt_office;
		$edudms_pt_bio = $user->edudms_pt_bio;
		$edudms_pt_cv = $user->edudms_pt_cv;
		$edudms_pt_courses = $user->edudms_pt_courses;
		$full_name = $first_name . ' ' . $last_name;
		$comma_name = $last_name . ', ' . $first_name;
		$edudms_pt_customshortfield1 = $user->edudms_pt_customshortfield1;
		$edudms_pt_customshortfield2 = $user->edudms_pt_customshortfield2;

		
	edudms_pt_person_setup_block('block_1_value', 'block_1_data_type', 'edudms_pt_pp_block_1_selection');
	edudms_pt_person_setup_block('block_2_value', 'block_2_data_type', 'edudms_pt_pp_block_2_selection');
	edudms_pt_person_setup_block('block_3_value', 'block_3_data_type', 'edudms_pt_pp_block_3_selection');
	edudms_pt_person_setup_block('block_4_value', 'block_4_data_type', 'edudms_pt_pp_block_4_selection');
	edudms_pt_person_setup_block('block_5_value', 'block_5_data_type', 'edudms_pt_pp_block_5_selection');
	edudms_pt_person_setup_block('block_6_value', 'block_6_data_type', 'edudms_pt_pp_block_6_selection');
	edudms_pt_person_setup_block('block_7_value', 'block_7_data_type', 'edudms_pt_pp_block_7_selection');
	edudms_pt_person_start();
	edudms_pt_wrap_it_start('edudms_pt_pp_name_and_title_layout_selection');
	edudms_pt_show_it($block_1_value, $block_1_data_type, 2 , get_option('edudms_pt_pp_block_1_linkon_selection'));
	edudms_pt_show_it($block_2_value, $block_2_data_type, 2 , get_option('edudms_pt_pp_block_2_linkon_selection'));
	edudms_pt_wrap_it_end('edudms_pt_pp_name_and_title_layout_selection');
	edudms_pt_show_it($block_3_value, $block_3_data_type, 2 , get_option('edudms_pt_pp_block_3_linkon_selection'));
	edudms_pt_show_it($block_4_value, $block_4_data_type, 2 , get_option('edudms_pt_pp_block_4_linkon_selection'));
	edudms_pt_show_it($block_5_value, $block_5_data_type, 2 , get_option('edudms_pt_pp_block_5_linkon_selection'));
	edudms_pt_show_it($block_6_value, $block_6_data_type, 2 , get_option('edudms_pt_pp_block_6_linkon_selection'));
	edudms_pt_show_it($block_7_value, $block_7_data_type, 2 , get_option('edudms_pt_pp_block_7_linkon_selection'));
	edudms_pt_person_end();
	}
	}
	?>







	
	
	
	</div> <!-- End entry content div -->

</article><!-- #post-## -->


		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>